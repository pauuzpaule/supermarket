import React from "react";
import ItemList from "./ItemList";
class Cart extends React.Component {


    render() {
        return(
            <div className="modal" id="cart-modal">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">

                        <div className="modal-header">
                            <h4 className="modal-title">Items In Cart</h4>
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <ItemList items={this.props.items}/>
                                </div>
                            </div>

                        </div>

                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger" data-dismiss="modal">Check Out</button>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Cart;