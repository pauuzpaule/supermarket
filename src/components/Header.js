import React from "react";
import "../App.css";
import $ from 'jquery';
import bootstrap from 'bootstrap';

import Cart from "./Cart";

class Header extends React.Component {

    showItemsInCart() {
        $("#cart-modal").modal('show');
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">Shopping</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <a onClick={() => this.showItemsInCart()} className="nav-link" href="#">
                                <i className="fa shopping-icon">&#xf07a;</i>
                                <span className='badge badge-warning' id='lblCartCount'> {this.props.cartItems.length} </span>
                            </a>
                        </li>
                    </ul>
                </div>
            <Cart items={this.props.cartItems}/>
            </nav>
        );
    }
}

export default Header;