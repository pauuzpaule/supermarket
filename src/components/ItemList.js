import React from "react";
import logo from "../logo.svg";
import "../App.css";
import Rating from "./common/Rating";

class Item extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <div className="item">
                <p className="text-center">
                    <img src="https://picsum.photos/200" alt="#"/>
                </p>
                <p className="name mt-3">{this.props.name}</p>
                <p className="price">UGX {this.props.price}</p>
                <Rating rating={this.props.rating}/>
                <button onClick={this.props.addToCart} className="btn btn-primary btn-sm btn-block mt-2">Add to cart</button>
            </div>
        );
    }
}

class ItemList extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            cart: []
        }
    }

    onAddToCart(item) {
        new Promise((resolve) => {
            this.setState({
                cart: [...this.state.cart, ...[item]]
            });
            resolve(1)
        }).then(() => {
            this.props.itemsChange(this.state.cart);
        });
    }

    renderItem(item) {
        return(
            <Item
                name={item.name}
                price={item.price}
                rating={item.rating}
                addToCart={() => this.onAddToCart(item)}
            />
        );
    }

    render() {
        return(
            <div className="row">
                {this.props.items.map((item, index) => <div key={index} className="col-md-3 mt-2">{this.renderItem(item)}</div>)}
            </div>
        );
    }

}

export default ItemList;