import React from "react";
import "../../App.css";


function Rating(props) {

    let stars = [];
    for (let i = 0; i < 5; i++) {
        stars.push(<span key={i} className={"fa fa-star " + (i < Math.round(props.rating) ? ' checked ' : '')}></span>);
    }
    return(
        <div className="d-inline rating">
            {stars}
        </div>
    );
}

export default Rating;