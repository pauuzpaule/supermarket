import React from 'react';
import Header from './components/Header';
import ItemList from "./components/ItemList";
import './App.css';
import "./loader";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cartItems: []
        }
    }

    onChangeCartItems(items) {
        this.setState({
            cartItems: items
        })
    }

  render() {

      let itemList= [
          {name: "Hp Envy 75", price: "1000000", rating: 3, image: "", id: 1},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 2},
          {name: "Hp Envy 75", price: "1000000", rating: 4, image: "", id: 3},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 4},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 5},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 6},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 7},
          {name: "Hp Envy 75", price: "1000000", rating: 4.3, image: "", id: 8},
      ];

    return(
        <div className="container">

          <div className="row">
            <div className="col-md-12">
               <Header cartItems={this.state.cartItems}/>
            </div>
          </div>

          <div className="row p-4">
            <div className="col-md-12">
               <ItemList items={itemList} itemsChange={(items) => this.onChangeCartItems(items)}/>
            </div>
          </div>

        </div>
    );
  }

}

export default App;
